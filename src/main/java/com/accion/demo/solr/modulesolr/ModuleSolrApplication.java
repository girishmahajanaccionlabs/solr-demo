package com.accion.demo.solr.modulesolr;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.Id;

import org.apache.solr.client.solrj.SolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Configuration
@EnableSolrRepositories(basePackages = "com.accion.demo.solr.modulesolr")
@SpringBootApplication
public class ModuleSolrApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModuleSolrApplication.class, args);
	}

	@Bean
	public SolrTemplate solrTemplate(SolrClient client) throws Exception {
		return new SolrTemplate(client);
	}
}


@SolrDocument(collection = "mycore")
class Manufacturer {

	@Id
	@Indexed(name = "id", type = "string")
	private String id;

	@Indexed(name = "compName_s", type = "string")
	private String companyName;


	@Indexed(name = "address_s", type = "string")
	private String address;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Manufacturer() {
		super();		//JPA needs this
	}
}

@RestController
class SearchController {
	@Autowired ManufactureRepository solrRepository;
	@GetMapping(path="/search")
	public List<Manufacturer> searchManufacturer(@RequestParam(name="q") String q ){	
		return Arrays.asList(solrRepository.findById(q).get());
	}
}

interface ManufactureRepository extends SolrCrudRepository<Manufacturer, String> {

	public Optional<Manufacturer> findById(String id);
	
	@Query("id:*?0* OR compName_s:*?0* OR address_s:*?0*")
	public Page<Manufacturer> findByCustomQuery(String searchTerm, Pageable pageable);

	//
	// If you can't solve the problem, kill it or comment it ;)
	//
	
//	@Query(name = "Manufacturer.findByNamedQuery")
//	public Page<Manufacturer> findByNamedQuery(String searchTerm, Pageable pageable);

}
